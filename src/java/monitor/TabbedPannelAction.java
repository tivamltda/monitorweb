/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monitor;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;

import comun.*;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.ServletContext;
import modelo.*;
import org.apache.struts2.util.ServletContextAware;

/**
 *
 * @author mlabrinb
 * Controller encargado de atender las peticiones del TabPanel de Transferencia de Archivos
 */
public class TabbedPannelAction extends ActionSupport  implements ServletContextAware {
    private String hora = "99:99:99";
    private String fecha = "01/01/1901";
    private String genera = "";
    private String valida = "";
    private String transf = "";
    private String sucursales = "";
    private String hlpsub = "";
    private String logerr = "";
    private int dias = 0;
    private int noleidosgen = 0;
    private int noleidosval = 0;
    private int noleidos = 0;
    private int tiempoalarma = 10000;

    private ServletContext servletContext;

    private List myList;
    private List<Sucursales> myListSuc;
    private List<Errores> myListErr;
    private Map<String, String> mylistAcc;
    
    @Override
    public String execute() throws Exception {
        //throw new UnsupportedOperationException("Not supported yet.");
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        
        return SUCCESS;
    }
    
    @Override
    public void setServletContext(final ServletContext context) {
       this.servletContext = context;
    }
    
    public void init() throws Exception {
    }
    
    /**
     * Atiende la petición de inicio de la vista principal para capturar el tiempo de repetición de la alarma
     * @return 
     */
    public String inicio() {
        tiempoalarma = 1000 * Integer.parseInt(servletContext.getInitParameter("tiempoalarma"));
        return SUCCESS;
    }
    
    /**
     * Atiende la petición de reprocesos en la generación
     * @return 
     */
    public String procgnl() {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        obj.AlertaError("c:\\temp\\monitorweb.log", sucursales);
        String[] sucursal = sucursales.split(",");
        for (int i = 0; i < sucursal.length; i++) { 
            String[] token = sucursal[i].split("-");
            dias = Integer.parseInt(token[1]);
            obj.PutProOpe(fechaproc, "TRT", "GNL", token[0], dias);
        }
        return SUCCESS;
    }
    
    /**
     * Atiende la petición de reprocesos en la validación
     * @return 
     */
    public String procval() {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        String[] sucursal = sucursales.split(",");
        for (int i = 0; i < sucursal.length; i++) 
            obj.PutProOpe(fechaproc, "TRT", "VAL", sucursal[i], dias);
            
        return SUCCESS;
    }

    public String geterrval() {
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        
       
        return SUCCESS;
    }

    public String geterrgen() {
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        
       
        return SUCCESS;
    }
    
    /**
     * Atiende la petición de lectura de los errores en la generación
     * @return 
     */
    public String marcaleidogen() {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        
        obj.UpdFlgSinLec(fechaproc, sucursales, "GNL");
       
        return SUCCESS;
        
    }
    
    /**
     * Atiende la petición de lectura de los errores en la validación
     * @return 
     */
    public String marcaleidoval() {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        obj.AlertaError("c:\\temp\\monitorwweb.log", "marcaleidoval - " + sucursales);
        obj.UpdFlgSinLec(fechaproc, sucursales, "VAL");
       
        return SUCCESS;
        
    }
    
    /**
     * Atiende la petición mostrar las sucursales con el subproceso de generación pendiente
     * @return 
     */
    public String vergenpend()
    {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        myListSuc = new ArrayList<Sucursales>();
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        myListSuc = obj.GetSucConGenPen(fechaproc, "GNL");
        
        return SUCCESS;
    }
    
    /**
     * Atiende la petición de mostrar las sucursales con el subproceso de generación OK
     * @return 
     */
    public String verallsuc()
    {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        myListSuc = new ArrayList<Sucursales>();
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        myListSuc = obj.GetAllSuc(fechaproc, "GNL");
        
        return SUCCESS;
    }

    /**
     * Atiende la petición de help del subproceso de validacion
     * @return 
     */
    public String helpvalida()
    {
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        hlpsub = obj.GetHelp("VAL");
        
        return SUCCESS;
    }

    /**
     * Atiende la petición de help del subproceso de generación
     * @return 
     */
    public String helpgenera()
    {
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        hlpsub = obj.GetHelp("GNL");
        
        return SUCCESS;
    }

    /**
     * Atiende la petición de help del subproceso de transferencia
     * @return 
     */
    public String helptra()
    {
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        hlpsub = obj.GetHelp("TRF");
        
        return SUCCESS;
    }

    /**
     * Atiende la petición de lectura de las sucursales con errores en el subproceso de validación y la visualización del log
     * @return 
     */
    public String vererrval()
    {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        myListErr = new ArrayList<Errores>();
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        myListErr = obj.GetSucConErrValAcc(fechaproc, "VAL");
        
        return SUCCESS;
    }

    /**
     * Atiende la petición de lectura de las sucursales con errores en el subproceso de generación y la visualización del log
     * @return 
     */
    public String vererrgen()
    {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        myListErr = new ArrayList<Errores>();
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        myListErr = obj.GetSucConErrValAcc(fechaproc, "GNL");
                
        return SUCCESS;
    }

    /**
     * Atiende la petición de lectura de las sucursales con errores en el subproceso de transferencia y la visualización del log
     * @return 
     */
    public String vertrapen()
    {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        myListSuc = new ArrayList<Sucursales>();
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        myListSuc = obj.GetSucConTraPen(fechaproc, "TRF", "VAL");
        
        return SUCCESS;
    }

    /**
     * Atiende la petición de la vista principal de mostrar los indicadores
     * @return 
     */
    public String transfer()
    {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        SimpleDateFormat formatfecha = new SimpleDateFormat("dd/MM/yyyy");
        fecha = formatfecha.format(hoy);
        
        SimpleDateFormat formathora = new SimpleDateFormat("HH:mm:ss");
        hora = formathora.format(hoy); 
        
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        
        
        Integer TotSuc = obj.GetTotSucursales();
        
        genera = obj.GetTotSubProceso(fechaproc, "GNL").toString() + " DE " + TotSuc.toString();
        valida = obj.GetTotSubProceso(fechaproc, "VAL").toString() + " DE " + TotSuc.toString();
        transf = obj.GetTotSubProceso(fechaproc, "TRF").toString() + " DE " + TotSuc.toString();
        
        noleidosgen = obj.GetTotErrSinLec(fechaproc, "GNL");
        noleidosval = obj.GetTotErrSinLec(fechaproc, "VAL");
        
        tiempoalarma = 1000 * Integer.parseInt(servletContext.getInitParameter("tiempoalarma"));
        
        return SUCCESS;
    }    

    /**
     * Atiende la petición de la vista pricipal para mostrar los errores no leidos por el operador
     * @return 
     */
    public String ObtenerNoLeidos () {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        
        noleidos = obj.GetTotErrSinLec(fechaproc, "GNL");
        if (noleidos == 0)
            noleidos = obj.GetTotErrSinLec(fechaproc, "VAL");
        
        return SUCCESS;
    }

    /**
     * Atiende la petición de la vista principal para mostrar los errores no leidos en la generación 
     * @return 
     */
    public String ObtenerNoLeidosGen () {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        
        noleidosgen = obj.GetTotErrSinLec(fechaproc, "GNL");
        
        return SUCCESS;
    }

    /**
     * Atiende la petición de la vista principal de mostrar los errores no leidos en el subproceso de validación
     * @return 
     */
    public String ObtenerNoLeidosVal () {
        Date hoy = new Date();
        
        SimpleDateFormat formatproceso = new SimpleDateFormat("dd/MM/yyyy");
        String fechaproc = formatproceso.format(hoy);
        
        transfermodel obj = new transfermodel((String) servletContext.getInitParameter("driver"),
                                              (String) servletContext.getInitParameter("connectionStrings"),
                                              (String) servletContext.getInitParameter("user"),
                                              (String) servletContext.getInitParameter("password"));
        
        noleidosval = obj.GetTotErrSinLec(fechaproc, "VAL");
        
        return SUCCESS;
    }
    
    public String replic() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //return SUCCESS;
    }    
    
    public String convenios() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //return SUCCESS;
    }    
    
    public String saptiendas() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //return SUCCESS;
    }    
    
    public String pedidos() throws Exception
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //return SUCCESS;
    }        
    
    public String getFecha() {
        return fecha;
    }
    
    public String getHora() {
        return hora;
    }

    public String getGenera() {
        return genera;
    }

    public String getTransf() {
        return transf;
    }

    public String getValida() {
        return valida;
    }

    public List getMyList() {
        return myList;
    }

    public List<Sucursales> getMyListSuc() {
        return myListSuc;
    }

    public String getSucursales() {
        return sucursales;
    }

    public void setSucursales(String sucursales) {
        this.sucursales = sucursales;
    }
    
    public String getHlpsub() {
        return hlpsub;
    }

    public String getLogerr() {
        return logerr;
    }

    public void setLogerr(String logerr) {
        this.logerr = logerr;
    }

    public Map<String, String> getMylistAcc() {
        return mylistAcc;
    }

    public List<Errores> getMyListErr() {
        return myListErr;
    }

    public int getNoleidosgen() {
        return noleidosgen;
    }

    public int getNoleidosval() {
        return noleidosval;
    }

    public int getNoleidos() {
        return noleidos;
    }

    public int getTiempoalarma() {
        return tiempoalarma;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

}
