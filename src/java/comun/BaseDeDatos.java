/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package comun;

import java.sql.*;

/**
 *
 * @author mlabrinb
 * Objeto para la Base de Datos
 */
public class BaseDeDatos {
    private String connectionStrings = "";
    private String user = "";
    private String password = "";
    private String Driver = "";
    
    private Connection conexion; 

    /**
     * Responsable de conectarse a la base de datos. El string de coneccion esta en connectionStrings 
     * retorna true si se pudo conectar
     *         false si no se pudo conectar
     * @return 
     */
    public boolean Conectar() {
        try
        {
           Class.forName(Driver);

           try {
              conexion = DriverManager.getConnection (connectionStrings, user, password);
           } catch (Exception err) {
                return false;
           }
           
        } catch (Exception e) {
            return false;
        } 
        return true;
    }

    /**
     * Desconecta de la base de datos
     */
    public void Desconectar() {
        try {
            conexion.close();
        } catch (Exception e) { }
    }

    /**
     * Getter para obtener la coneccion a la base
     * @return 
     */
    public Connection getConexion() {
        return conexion;
    }

    /**
     * Getter para obtener el Driver de Conexion 
     * @return 
     */
    public String getDriver() {
        return Driver;
    }

    /**
     * Setter para el Driver de Connexion
     * @param Driver driver de coneccion
     */
    public void setDriver(String Driver) {
        this.Driver = Driver;
    }

    /**
     * Getter pata el String de Conexion
     * @return 
     */
    public String getConnectionStrings() {
        return connectionStrings;
    }

    /**
     * Setter para el string de conneccion
     * @param connectionStrings string de coneccion
     */
    public void setConnectionStrings(String connectionStrings) {
        this.connectionStrings = connectionStrings;
    }

    /**
     * Getter para obtener la clave
     * @return 
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter para la clave
     * @param password clave
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter para obtener el Usuario de la Base
     * @return 
     */
    public String getUser() {
        return user;
    }

    /**
     * Setter para el Usuario de la Base
     * @param user usuario
     */
    public void setUser(String user) {
        this.user = user;
    }

   
}
