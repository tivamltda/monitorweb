/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package comun;

/**
 *
 * @author mlabrinb
 * Objeto de Negocio para la entidad de negocio Errores en el Proceso
 */
public class Errores {
    private String id_suc;
    private String nom_suc;
    private String log_suc;
    private int flg_lec;
    private int numdias;

    /**
     * Getter pa el Codigo de la Sucursal
     * @return 
     */
    public String getId_suc() {
        return id_suc;
    }

    /**
     * Setter para el Codigo de la Sucursal
     * @param id_suc codigo de la sucursal
     */
    public void setId_suc(String id_suc) {
        this.id_suc = id_suc;
    }

    /**
     * Getter para el Nombre de la Sucursal
     * @return 
     */
    public String getNom_suc() {
        return nom_suc;
    }

    /**
     * Setter para el Nombre de la sucursal
     * @param nom_suc nombre de la sucursal
     */
    public void setNom_suc(String nom_suc) {
        this.nom_suc = nom_suc;
    }

    /**
     * Getter para el Log de la Sucursal
     * @return 
     */
    public String getLog_suc() {
        return log_suc;
    }

    /**
     * Setter para el Log de la Sucursal
     * @param log_suc log con el resultado del proceso
     */
    public void setLog_suc(String log_suc) {
        this.log_suc = log_suc;
    }

    /**
     * Getter para el Flag de Lectura
     * @return 
     */
    public int getFlg_lec() {
        return flg_lec;
    }

    /**
     * Setter para el Flag de la Lectura
     * @param flg_lec 1 si lo leyo, 0 sino
     */
    public void setFlg_lec(int flg_lec) {
        this.flg_lec = flg_lec;
    }

    /**
     * Getter para el Numero de días
     * @return 
     */
    public int getNumdias() {
        return numdias;
    }

    /**
     * Setter para el numero de días
     * @param numdias numero de días del reproceso
     */
    public void setNumdias(int numdias) {
        this.numdias = numdias;
    }
    
}
