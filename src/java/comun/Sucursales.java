/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package comun;

/**
 *
 * @author mlabrinb
 * Objeto de Negocio para la entidad de negocio Sucursales (Locales)
 */
public class Sucursales {
    private String id_suc;
    private String nom_suc;
    private String est_trt;
    private int    numdias;

    /**
     * Getter para el Codigo de Sucursal
     */
    public String getId_suc() {
        return id_suc;
    }

    /**
     * Setter para el Codigo de la Sucursal
     * @param id_suc codigo de sucursal
     */
    public void setId_suc(String id_suc) {
        this.id_suc = id_suc;
    }

    /**
     * Getter para el Nombre de la Sucursal
     */
    public String getNom_suc() {
        return nom_suc;
    }

    /**
     * Setter para el Nombre de la Sucursal
     * @param nom_suc nombre de la sucursal
     */
    public void setNom_suc(String nom_suc) {
        this.nom_suc = nom_suc;
    }

    /**
     * Getter para el estado de la transmision de la sucursal
     */
    public String getEst_trt() {
        return est_trt;
    }

    /**
     * Setter para el estado de la transmision de la sucursal
     * @param est_trt estado de la transmision
     */
    public void setEst_trt(String est_trt) {
        this.est_trt = est_trt;
    }

    /**
     * Getter para el número de días del reproceso
     */
    public int getNumdias() {
        return numdias;
    }

    /**
     * Setter para el número de días del reproceso
     * @param numdias numero de días
     */
    public void setNumdias(int numdias) {
        this.numdias = numdias;
    }
    
}
