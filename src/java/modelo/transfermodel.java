/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import comun.*;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;

/**
 *
 * @author mlabrinb
 * Contiene todas las acciones del Modelo
 */

public class transfermodel {
    private BaseDeDatos sql;
    private Boolean CnnExt;

    /**
     * Entrega el Id del Proceso
     * @param conexion handle de la coneccion a la base de datos
     * @param nompro numero de proceso
     * @return Id del Proceso
     */
    
    private int GetId_Pro (Connection conexion, String nompro) {
        int Id_Pro = -1;
        try {
            Statement s = conexion.createStatement(); 
            String query = "select id_pro from pro where nompro = '" + nompro + "'";
            ResultSet rs = s.executeQuery (query);
            while (rs.next()) Id_Pro = rs.getInt("id_pro");
            rs.close();
            s.close();
        } catch (Exception e) {
            AlertaError("c:\\temp\\monitorweb.log", "GetId_Pro - " + e.getMessage());
        }
        return Id_Pro;
    }
    
    /**
     * Ontiene del Id del Subproceso
     * @param conexion handle de la coneccion a la base de datos
     * @param Id_Pro Id del Proceso
     * @param nomsub Nombre del Subproceso
     * @return Id del Subproceso
     */
    private int GetId_SubPro (Connection conexion, int Id_Pro, String nomsub) {
        int Id_Sub = -1;
        try {
            Statement s = conexion.createStatement(); 
            String query = "select id_sub from subpro where id_pro = " + Id_Pro + " and nomsub = '" + nomsub + "'";
            ResultSet rs = s.executeQuery (query);
            while (rs.next()) Id_Sub = rs.getInt("id_sub");
            rs.close();
            s.close();
        } catch (Exception e) {
            AlertaError("c:\\temp\\monitorweb.log", "GetId_SubPro - " + e.getMessage());
        }
        return Id_Sub;
    }
    
    /**
     * Obtiene el numero de proceso
     * @param conexion handle de la coneccion a la base de datos
     * @param fecpro fecha de proceso
     * @param Id_Pro Id del proceso
     * @param Id_Sub Id del subproceso
     * @param sucursal Codigo de la Sucursal
     * @return  Numero del Proceso
     */
    private int Get_NumPro (Connection conexion, String fecpro, int Id_Pro, int Id_Sub, String sucursal) {
        int NumPro = 0;
        try {
            Statement s = conexion.createStatement(); 
            String query =  "select numpro from evtsucpro " +
                            "where  fecpro = '" + fecpro +"' " +
                            "and    id_pro = " + Id_Pro + " " + 
                            "and    id_sub = " + Id_Sub + " " +
                            "and    id_suc = '" + sucursal + "' " +
                            "and    estpro != 9 " +
                            "order by numpro desc";
            ResultSet rs = s.executeQuery (query);
            while (rs.next()) {
                NumPro = rs.getInt("numpro");
                break;
            }
            rs.close();
            s.close();
        } catch (Exception e) {
            AlertaError("c:\\temp\\monitorweb.log", "Get_NumPro - " + e.getMessage());
        }
        return NumPro;
    }
    
    /**
     * Obtiene el Numero de proceso para los procesos ordenados por el operador
     * @param conexion handle de la coneccion a la base de datos
     * @param fecpro fecha de proceso
     * @param Id_Pro Id del Proceso
     * @param Id_Sub Id del subproceso
     * @param sucursal Codigo de la sucursal
     * @return Numero de proceso
     */
    private int Get_NumProOpe (Connection conexion, String fecpro, int Id_Pro, int Id_Sub, String sucursal) {
        int NumPro = 0;
        try {
            Statement s = conexion.createStatement(); 
            String query =  "select numpro from ProOpe " +
                            "where  fecpro = '" + fecpro +"' " +
                            "and    id_pro = " + Id_Pro + " " + 
                            "and    id_sub = " + Id_Sub + " " +
                            "and    id_suc = '" + sucursal + "' " +
                            "order by numpro desc";
            ResultSet rs = s.executeQuery (query);
            while (rs.next()) {
                NumPro = rs.getInt("numpro");
                break;
            }
            rs.close();
            s.close();
        } catch (Exception e) {
            AlertaError("c:\\temp\\monitorweb.log", "Get_NumPro - " + e.getMessage());
        }
        return NumPro;
    }
    
    /**
     * Define los parametros de la coneccion a la base de datos
     * @param drv driver
     * @param cnn string de coneccion
     * @param usr usuario
     * @param pwd clave
     */
    public transfermodel(String drv, String cnn, String usr, String pwd) {
        this.sql = new BaseDeDatos();
        
        this.sql.setDriver(drv);
        this.sql.setConnectionStrings(cnn);
        this.sql.setUser(usr);
        this.sql.setPassword(pwd);
        this.CnnExt = true;
        
        if (cnn.equals("jdbc:oracle:thin:@localhost:1521:XE")) this.CnnExt = false;
    }

    /**
     * Obtiene la ayuda del subproceso
     * @param subproceso nombre del subproceso
     * @return string de ayuda
     */
    public String GetHelp(String subproceso) {
        String resultado = "";
        
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                Statement s = conexion.createStatement(); 
                String query = "select * " +
                               "from   subpro " +
                               "where  nomsub = '" + subproceso + "'";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) resultado = rs.getString("hlpsub");
            } catch (Exception e) {
                resultado = e.getMessage();
            }    
            sql.Desconectar();
        }
        
        return resultado;
    }
    
    /**
     * Inserta un proceso ordenado por el operador
     * @param fecha fecha del proceso
     * @param proceso nombre del proceso
     * @param subproceso nombre del subproceso
     * @param id_suc codigo de la sucursal
     * @param numdias numero de días a procesar
     */
    public void PutProOpe(String fecha, String proceso, String subproceso, String id_suc, int numdias) {
        
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                int id_pro = GetId_Pro(conexion, proceso);
                int id_sub = GetId_SubPro(conexion, id_pro, subproceso);
                boolean xis_proope = false;
                
                Statement s = conexion.createStatement(); 
                String query = "select * " +
                               "from   proope " +
                               "where  fecpro = '" + fecha + "' " +
                               "and    id_pro = " + id_pro + " " +
                               "and    id_sub = " + id_sub + " " +
                               "and    id_suc = '" + id_suc + "' " +
                               "and    estpro = 0";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) xis_proope = true;                
                rs.close();
                if (!xis_proope) {
                    int numpro = Get_NumProOpe(conexion, fecha, id_pro, id_sub, id_suc);
                    numpro++;

                    String update = "insert into proope (fecpro, id_pro, id_sub, id_suc, numpro, estpro, numdia) values (?, ?, ?, ?, ?, 0, ?)";
                    PreparedStatement stmtu = conexion.prepareStatement(update); 
                    stmtu.setString(1, fecha);
                    stmtu.setInt(2, id_pro);
                    stmtu.setInt(3, id_sub);
                    stmtu.setString(4, id_suc);
                    stmtu.setInt(5, numpro);
                    stmtu.setInt(6, numdias);
                    stmtu.executeUpdate();
                    stmtu.close();
                }
            } catch (Exception e) {
                AlertaError ("c:\\temp\\monitorweb.log", e.getMessage());
                return;
            }    
            sql.Desconectar();
        }
        
    }
    
    /**
     * Obtiene el total de las sucursales activas
     * @return Total de Sucursales
     */
    public Integer GetTotSucursales() {
        Integer Total = -1;
        
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                Statement s = conexion.createStatement(); 
                String query = "select count(*) total from suc where sucoff = 0";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) Total = rs.getInt("total");
            } catch (Exception e) {
                Total = -2;
            }    
            sql.Desconectar();
        }
        
        return Total;
    }

    /**
     * Obtiene el total de subprocesos finalizados
     * @param fecha fecha de proceso
     * @param subproceso nombre del subproceso
     * @return Total de subproceso finalizados
     */
    public Integer GetTotSubProceso(String fecha, String subproceso) {
        Integer Total = -1;
        
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                Statement s = conexion.createStatement(); 
                String query = "select count(*) total " +
                               "from   evtsucpro a1 " +
                               "      ,subpro    a2 " +
                               "where  a1.fecpro = '" + fecha +"' " +
                               "and    a1.estpro = 2 " +
                               "and    a2.id_sub = a1.id_sub " +
                               "and    a2.id_pro = a1.id_pro " +
                               "and    a2.nomsub = '" + subproceso + "'";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) Total = rs.getInt("total");
            } catch (Exception e) {
                Total = -2;
            }    
            sql.Desconectar();
        }
        
        return Total;        
    }

    /**
     * Obtiene el total de sucursales con errores sin lectura por parte del operador
     * @param fecha fecha del proceso
     * @param subproceso nombre del subproceso
     * @return total de sucursales con errores
     */
    public Integer GetTotErrSinLec(String fecha, String subproceso) {
        Integer Total = -1;
        
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                Statement s = conexion.createStatement(); 
                String query = "select count(*) total " +
                               "from   evtsucpro a1 " +
                               "      ,subpro    a2 " +
                               "      ,suc       a3 " +
                               "where  a1.fecpro = '" + fecha +"' " +
                               "and    a1.estpro = 2 " +
                               "and    a2.id_sub = a1.id_sub " +
                               "and    a2.id_pro = a1.id_pro " +
                               "and    a2.nomsub = '" + subproceso + "' " +
                               "and    a3.id_suc = a1.id_suc " +
                               "and    a3.sucoff = 0 " +
                               "and    exists (select 1 " +
                               "               from   detevtsuc det " +
                               "               where  det.fecpro = a1.fecpro " +
                               "               and    det.id_suc = a1.id_suc " +
                               "               and    det.id_pro = a1.id_pro " +
                               "               and    det.id_sub = a1.id_sub " +
                               "               and    det.numpro = a1.numpro " +
                               "               and    det.tipevt = 4 " +
                               "               and    det.flglec = 0)";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) Total = rs.getInt("total");
            } catch (Exception e) {
                Total = -2;
            }    
            sql.Desconectar();
        }
        
        return Total;        
    }

    /**
     * Actualiza el flag de lectura por parte del operador
     * @param fecha fecha de proceso
     * @param id_suc codigo de la sucursal
     * @param subproceso nombre del subproceso
     * @return True si lo pudo actualizar, False sino
     */
    public Boolean UpdFlgSinLec(String fecha, String id_suc, String subproceso) {
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                int id_pro = GetId_Pro(conexion, "TRT");
                int id_sub = GetId_SubPro(conexion, id_pro, subproceso);
                int numpro = Get_NumPro(conexion, fecha, id_pro, id_sub, id_suc);
                
                String update = "update detevtsuc set flglec = 1 where fecpro = ? and id_pro = ? and id_sub = ? and id_suc = ? and numpro = ? and tipevt = 4";
                PreparedStatement stmtu = conexion.prepareStatement(update); 
                stmtu.setString(1, fecha);
                stmtu.setInt(2, id_pro);
                stmtu.setInt(3, id_sub);
                stmtu.setString(4, id_suc);
                stmtu.setInt(5, numpro);
                stmtu.executeUpdate();
                stmtu.close();
            } catch (Exception e) {
                AlertaError ("c:\\temp\\monitorweb.log", e.getMessage());
                return false;
            }    
            sql.Desconectar();
        }
        
        return true;        
    }

    /**
     * Entrega un ArrayList de Sucursales que tienen el proceso de generación pendiente de procesar
     * @param fecha fecha de proceso
     * @param subproceso nombre del subproceso
     * @return Lista de Sucursales
     */
    public ArrayList<Sucursales> GetSucConGenPen (String fecha, String subproceso) {
        ArrayList<Sucursales> suc = new ArrayList<Sucursales>();
        
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                Statement s = conexion.createStatement(); 
                String query = "select suc.id_suc, suc.nomsuc " +
                               "from   suc, sucpro, subpro " +
                               "where  suc.sucoff = 0 " +
                               "and    sucpro.id_suc = suc.id_suc " +
                               "and    subpro.id_pro = sucpro.id_pro " +
                               "and    subpro.nomsub = '" + subproceso + "' " +
                               "and    not exists (select 1 " +
                               "                   from   evtsucpro evt " +
                               "                   where  evt.fecpro = '" + fecha +"' " +
                               "                   and    evt.id_suc = suc.id_suc " +
                               "                   and    evt.id_pro = subpro.id_pro " +
                               "                   and    evt.id_sub = subpro.id_sub " +
                               "                   and    evt.estpro = 2) " +
                               "order by suc.nomsuc";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) {
                    Sucursales item = new Sucursales();
                    item.setId_suc(rs.getString("id_suc"));
                    item.setNom_suc(rs.getString("nomsuc"));
                    item.setNumdias(GetNumDias(conexion, rs.getString("id_suc")));
                    suc.add(item);
                }
                rs.close();
                s.close();
            } catch (Exception e) {
                suc = null;
            }    
            sql.Desconectar();
        }
        
        return suc;
    }

    /**
     * Entrega un ArrayList con todas las Sucursales que tiene el subproceso finalizado 
     * @param fecha fecha de proceso
     * @param subproceso nombre del subproceso
     * @return Lista de sucursales
     */
    public ArrayList<Sucursales> GetAllSuc (String fecha, String subproceso) {
        ArrayList<Sucursales> suc = new ArrayList<Sucursales>();
        AlertaError ("c:\\temp\\monitorweb.log", "GetAllSuc");
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                Statement s = conexion.createStatement(); 
                String query = "select suc.id_suc, suc.nomsuc, evt.estpro \n" +
                                "from   suc\n" +
                                "join   sucpro\n" +
                                "on     sucpro.id_suc = suc.id_suc\n" +
                                "join   subpro\n" +
                                "on     subpro.id_pro = sucpro.id_pro\n" +
                                "left join evtsucpro evt\n" +
                                "on     evt.fecpro = '" + fecha + "'\n" +
                                "and    evt.id_suc = suc.id_suc\n" +
                                "and    evt.id_pro = subpro.id_pro\n" +
                                "and    evt.id_sub = subpro.id_sub\n" +
                                "and    evt.estpro = 2\n" +
                                "where  suc.sucoff = 0\n" +
                                "and    subpro.nomsub = '" + subproceso + "' \n" +
                                "order by suc.nomsuc";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) {
                    Sucursales item = new Sucursales();
                    item.setId_suc(rs.getString("id_suc"));
                    item.setNom_suc(rs.getString("nomsuc"));
                    String est_trt = rs.getInt("estpro") > 0 ? "OK" : "Pendiente";
                    item.setNumdias(GetNumDias(conexion, rs.getString("id_suc")));
                    item.setEst_trt(est_trt);
                    suc.add(item);
                }
                rs.close();
                s.close();
            } catch (Exception e) {
                suc = null;
            }    
            sql.Desconectar();
        }
        
        return suc;
    }

    /**
     * Entrega un ArrayList con todas las Sucursales que tienen errores de validación 
     * @param fecha fecha de prceso
     * @param subproceso nombre del subproceso
     * @return Lista de sucursales
     */
    public ArrayList<Sucursales> GetSucConErrVal (String fecha, String subproceso) {
        ArrayList<Sucursales> suc = new ArrayList<Sucursales>();
        
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                Statement s = conexion.createStatement(); 
                String query = "select suc.id_suc, suc.nomsuc " +
                               "from   suc, sucpro, subpro, evtsucpro evt " +
                               "where  suc.sucoff = 0 " +
                               "and    sucpro.id_suc = suc.id_suc " +
                               "and    subpro.id_pro = sucpro.id_pro " +
                               "and    subpro.nomsub = '" + subproceso + "' " +
                               "and    evt.fecpro    = '" + fecha +"' " +
                               "and    evt.id_suc    = suc.id_suc " +
                               "and    evt.id_pro    = subpro.id_pro " +
                               "and    evt.id_sub    = subpro.id_sub " +
                               "and    evt.estpro    = 2 " +
                               "and    exists (select 1 " +
                               "               from   detevtsuc det " +
                               "               where  det.fecpro = evt.fecpro " +
                               "               and    det.id_suc = evt.id_suc " +
                               "               and    det.id_pro = evt.id_pro " +
                               "               and    det.id_sub = evt.id_sub " +
                               "               and    det.tipevt = 4) " +
                               "order by suc.nomsuc";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) {
                    Sucursales item = new Sucursales();
                    item.setId_suc(rs.getString("id_suc"));
                    item.setNom_suc(rs.getString("nomsuc"));
                    suc.add(item);
                }
            } catch (Exception e) {
                suc = null;
            }    
            sql.Desconectar();
        }
        
        return suc;
    }    

    /**
     * Entrega un ArrayList con todas las Sucursales que tiene errores en el subproceso de validacion
     * @param fecha fecha de proceso
     * @param subproceso nombre del subproceso
     * @return Lista de Errores
     */
    public ArrayList<Errores>  GetSucConErrValAcc (String fecha, String subproceso) {
        ArrayList<Errores> suc = new ArrayList<Errores>();
        
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                Statement s = conexion.createStatement(); 
                String query = "select suc.id_suc, suc.nomsuc, evt.id_pro, evt.id_sub, evt.numpro, det.flglec " +
                               "from   suc, sucpro, subpro, evtsucpro evt, detevtsuc det " +
                               "where  suc.sucoff = 0 " +
                               "and    sucpro.id_suc = suc.id_suc " +
                               "and    subpro.id_pro = sucpro.id_pro " +
                               "and    subpro.nomsub = '" + subproceso + "' " +
                               "and    evt.fecpro    = '" + fecha +"' " +
                               "and    evt.id_suc    = suc.id_suc " +
                               "and    evt.id_pro    = subpro.id_pro " +
                               "and    evt.id_sub    = subpro.id_sub " +
                               "and    evt.estpro    = 2 " +
                               "and    det.fecpro    = evt.fecpro " +
                               "and    det.id_suc    = evt.id_suc " +
                               "and    det.id_pro    = evt.id_pro " +
                               "and    det.id_sub    = evt.id_sub " +
                               "and    det.numpro    = evt.numpro " +
                               "and    det.tipevt    = 4 " +
                               "order by suc.nomsuc";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) {
                    int id_pro = rs.getInt("id_pro");
                    int id_sub = rs.getInt("id_sub");
                    String id_suc = rs.getString("id_suc");
                    String nom_suc = rs.getString("nomsuc");
                    int numpro = rs.getInt("numpro");
                    int flglec = rs.getInt("flglec");
                    
                    try {
                        Statement s2 = conexion.createStatement(); 
                        String query2 = "select linlog " +
                                        "from   logdetevt " +
                                        "where  fecpro = '" + fecha + "' " +
                                        "and    id_pro = " + id_pro + " " +
                                        "and    id_sub = " + id_sub + " " +
                                        "and    id_suc = '" + id_suc + "' " +
                                        "and    numpro = " + numpro + " " +
                                        "order by linlog";
                                
                        String logerr = "";
                        ResultSet rs2 = s2.executeQuery (query2);
                        while (rs2.next()) 
                            if (rs2.getString("linlog") != null) {
                                logerr += rs2.getString("linlog");
                                logerr += "<br/>";
                            }     
                        
                        Errores item = new Errores();
                        item.setId_suc(id_suc);
                        item.setNom_suc(nom_suc);
                        item.setLog_suc(logerr);
                        item.setFlg_lec(flglec);
                        item.setNumdias(GetNumDias(conexion, id_suc));
                        suc.add(item);
                        AlertaError ("c:\\temp\\monitorweb.log", "ArrayList suc: " + nom_suc);
                        s2.close();
                    }
                    catch (Exception e2) {
                        AlertaError ("c:\\temp\\monitorweb.log", "e2: " + e2.getMessage());
                        suc = null;
                        break;
                    }
                }
                s.close();
            } catch (Exception e) {
                suc = null;
                AlertaError ("c:\\temp\\monitorweb.log", "e: " + e.getMessage());
            }    
            sql.Desconectar();
        }
        
        return suc;
    }    

    /**
     * Entrega un ArrayList con todas las Sucursales que tienen transmisiones pendientes
     * @param fecha fecha de proceso
     * @param subproceso nombre del subproceso
     * @param subprocesoant nombre del subproceso anterior
     * @return Lista de Sucursales
     */
    public ArrayList<Sucursales> GetSucConTraPen (String fecha, String subproceso, String subprocesoant) {
        ArrayList<Sucursales> suc = new ArrayList<Sucursales>();
        
        if (sql.Conectar()) {
            Connection conexion = sql.getConexion();
            try {
                Statement s = conexion.createStatement(); 
                String query =  "select suc.id_suc, suc.nomsuc, evt.estpro \n" +
                                "from   suc\n" +
                                "join   sucpro\n" +
                                "on     sucpro.id_suc = suc.id_suc\n" +
                                "join   subpro\n" +
                                "on     subpro.id_pro = sucpro.id_pro\n" +
                                "left join evtsucpro evt\n" +
                                "on     evt.fecpro = '" + fecha +"'\n" +
                                "and    evt.id_suc = suc.id_suc\n" +
                                "and    evt.id_pro = subpro.id_pro\n" +
                                "and    evt.id_sub = subpro.id_sub\n" +
                                "and    evt.estpro = 2\n" +
                                "where  suc.sucoff = 0\n" +
                                "and    subpro.nomsub = '" + subproceso + "' \n" +
                                "order by suc.nomsuc";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) {
                    Sucursales item = new Sucursales();
                    item.setId_suc(rs.getString("id_suc"));
                    item.setNom_suc(rs.getString("nomsuc"));
                    String est_trt = rs.getInt("estpro") > 0 ? "OK" : "Pendiente";
                    item.setEst_trt(est_trt);
                    suc.add(item);
                }
            } catch (Exception e) {
                suc = null;
            }    
            sql.Desconectar();
        }
        
        return suc;
    }    
    
    /**
     * Graba un alerta de error
     * @param filelog nombre del archivo de log
     * @param mensaje mensaje a logear
     */
    public void AlertaError(String filelog, String mensaje) {
        
        Date hoy = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fecha = format.format(hoy); /* Formato con que se debe grabar la fecha y hora de lo escrito en el log*/
        FileWriter fichero = null;

        /* Agregar al archivo de log filelog un mensaje. Anteponer fecha y hora del mensaje */
        try {
            File folder = new File(filelog);
            if (folder.exists()) fichero = new FileWriter(filelog, true);
            else fichero = new FileWriter(filelog);
            PrintWriter pw = new PrintWriter(fichero);
            pw.println(fecha + " " + mensaje);
            
        } catch (Exception e) {
            System.out.println(fecha + " " + e.getMessage());
        } finally {
           try {
               if (null != fichero) fichero.close();
           } catch (Exception e2) {
               System.out.println(fecha + " " + e2.getMessage());
           }
        }
        
    }

    /**
     * Obtiene e numero de días del reproceso desde la tabla operador.datos_traer@edbst000
     * @param conexion handle de conecciona la base de datos
     * @param sucursal codigo de sucursal
     * @return Numero de días
     */
    private int GetNumDias(Connection conexion, String sucursal) {
        int numdias = 1;
        
        if (this.CnnExt) {
            try {
                Statement s = conexion.createStatement(); 
                String query =  "select nu_dias\n" +
                                "from   operador.datos_traer@edbst000\n" +
                                "where  co_local = '" + sucursal + "'";
                ResultSet rs = s.executeQuery (query);
                while (rs.next()) {
                    numdias = rs.getInt("nu_dias");
                }
                rs.close();
                s.close();
            } catch (Exception e) {
                AlertaError ("c:\\temp\\monitorweb.log", e.getMessage());
            }    
        }
        
        return numdias;
    }
    
}
