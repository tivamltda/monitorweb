<%-- 
    Document   : ErrValidacion
    Created on : 15-abr-2013, 11:52:00
    Author     : mlabrinb
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            @import url(<s:url value="/css/monitor.css"/>);
            @import url(<s:url value="/DataTables-1.9.4/media/css/jquery.dataTables_themeroller.css"/>);
        </style>       
	<link href="http://code.jquery.com/ui/1.9.0/themes/redmond/jquery-ui.css" rel="stylesheet" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.js"></script>
	<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.min.js"></script>
        <script type="text/javascript" language="javascript" src="<s:url value="/DataTables-1.9.4/media/js/jquery.dataTables.js"/>"></script>
        
        <script type="text/javascript">
            function fnFormatDetails ( oTable, nTr )
            {
                var aData = oTable.fnGetData( nTr );
                
                var sOut = "<div id='reporte' style='position:relative; width:100%; height:200px; overflow:auto; left: 0px; top: 0px;'>";
                sOut += "<b>Log Proceso:</b><br/><br/>";
                sOut += aData[3];
                sOut += "</div>";

                return sOut;
            }
            
            $(document).ready(function() {
                /*
                 * Insert a 'details' column to the table
                 */
                var nCloneTh = document.createElement( 'th' );
                var nCloneTd = document.createElement( 'td' );
                nCloneTd.innerHTML = '<img src="<s:url value="/DataTables-1.9.4/examples/examples_support/details_open.png"/>"/>';
                nCloneTd.className = "center";

                $('#TblGeneracion thead tr').each( function () {
                    this.insertBefore( nCloneTh, this.childNodes[0] );
                } );

                $('#TblGeneracion tbody tr').each( function () {
                    this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
                } );
                
                $(".check_todos").click(function(event){
                     if($(this).is(":checked")) {
                                $(".cdkgen:checkbox:not(:checked)").attr("checked", "checked");
                         }else{
                                 $(".cdkgen:checkbox:checked").removeAttr("checked");
                         }
                   });
   
                var oTable = $('#TblGeneracion').dataTable({
                    "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 0, 4, 5 ] },
                        { "bVisible": false, "aTargets": [ 3 ] }
                    ],
                    "aaSorting": [[1, 'asc']],                    
                    "oLanguage": {
                        "oPaginate": {
                            "sFirst": "Primera",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior",
                            "sLast": "Ultima"
                        },
                        "sInfo": "Registros encontrados _TOTAL_, mostrando desde el _START_ al _END_",
                        "sEmptyTable": "No existen registros a mostrar",
                        "sSearch": "Filtro de Busqueda:",
                        "sLengthMenu": "Mostrando _MENU_ registros por p�gina",
                        "sInfoFiltered": "(filtrados desde _MAX_ registros)"
                    },
                    "sScrollY": "290px",
                    "bPaginate": false,
                    "bJQueryUI": true,
                    "bFilter": true
                });
                
                /* Add event listener for opening and closing details
                 * Note that the indicator for showing which row is open is not controlled by DataTables,
                 * rather it is done here
                 */
                $('#TblGeneracion tbody td img').live('click', function () {
                    var nTr = $(this).parents('tr')[0];
                    if ( oTable.fnIsOpen(nTr) )
                    {
                        /* This row is already open - close it */
                        this.src = "<s:url value="/DataTables-1.9.4/examples/examples_support/details_open.png"/>";
                        oTable.fnClose( nTr );
                    }
                    else
                    {
                        /* Open this row */
                        this.src = "<s:url value="/DataTables-1.9.4/examples/examples_support/details_close.png"/>";
                        oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
                        var aData = oTable.fnGetData( nTr );
                        $.ajax({
                           type:'POST',
                           url: "<s:url value="/MarcarErrLeidoGen"/>",
                           data: {
                             sucursales: aData[1]
                           }
                        });                        
                    }
                } );
                
                
            });
            
            function SoloNumeros(e) {
                //if (oObjeto.value.substring(oObjeto.value.length-1,oObjeto.value.length) >= 0 ) {}
                //else { oObjeto.value = oObjeto.value.substring(0,oObjeto.value.length-1); }	
                tecla = (document.all) ? e.keyCode : e.which; // 2
                if (tecla == 8 || tecla == 0) return true; // 3
                patron = /\d/; // 4
                te = String.fromCharCode(tecla); // 5
                return patron.test(te); // 6
            }

            function OkSelSuc(frm) {
                var ExistenSel = false;
                var sucursales = "";
                $(".cdkgen:checkbox:checked").each(function(){
                    ExistenSel = true;
                    if (sucursales !== "") sucursales += ",";
                    sucursales += $(this).val();
                    if (parseInt($("#numdias"+$(this).val()).val()) < 1) {
                        alert('Debe indicar un n�mero de d�as igual o superior a 1');
                        $("#numdias"+$(this).val()).focus();
                        return false;                    
                    }
                    sucursales += "-" + $("#numdias"+$(this).val()).val();
                });
                if (!ExistenSel) {
                    alert('Debe seleccionar alguna o varias sucursales');
                    return false;
                }
                $.ajax({
                   type:'POST',
                   url: "<s:url value="/doErrGen"/>",
                   data: {
                     sucursales: sucursales
                   },
                   success: function (resultado) {
                      alert('Reproceso enviado exitosamente');
                   }
                });                        
            }
        </script>
    </head>
    <body>
        <s:form action="doErrGen" method="POST" id="frm" name="frm">
            <s:hidden id="sucursales" name="sucursales" value="" />
        </s:form>
        <table width="100%">
        <table width="100%" border="0">    
            <tbody>
                <tr>
                    <td width="60%" style="text-align:left">
                        <input name="Todos" type="checkbox" value="1" class="check_todos"/>Seleccionar todos
                    </td>                    
                    <td width="40%" style="text-align:right">
                        <input type="button" id="btnOk" onclick="javascript:OkSelSuc();" value="ejecutar" />
                    </td>
                </tr>            
            </tbody>
        </table>
        </table>
        <table width="100%" class="display" id="TblGeneracion">
            <thead>
                <tr>
                    <th style="text-align: left">C&oacute;digo</th>
                    <th style="text-align: left">Nombre</th>
                    <th style="text-align: left">Log</th>
                    <th style="text-align: center">Status</th>
                    <th style="text-align: center">&nbsp;</th>
                    <th style="text-align: center">D&iacute;as</th>
                </tr>
            </thead>
            <tbody>                
            <s:iterator value="myListErr">
                <tr>
                    <td width="15%" style="text-align: left"><s:property value="id_suc" /></td>
                    <td width="50%" style="text-align: left"><s:property value="nom_suc" /></td>
                    <td style="text-align: left"><s:property value="log_suc" escapeHtml="false" /></td>
                    <td width="20%" style="text-align: center">
                        <s:if test="flg_lec > 0">Leido</s:if>
                        <s:else>No Leido</s:else>
                    </td>                    
                    <td width="5%" style="text-align: center">
                        <input type="checkbox" id="locales" value="<s:property value="id_suc" />" class="cdkgen" />
                    </td>
                    <td width="10%" style="text-align: center">
                        <input type="text" id="numdias<s:property value="id_suc" />" value="<s:property value="numdias" />" size="2" onkeypress="return SoloNumeros(event)" />
                    </td>
                </tr>
            </s:iterator>
                    
            </tbody>
        </table>
    </body>
</html>
