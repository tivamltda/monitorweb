<%-- 
    Document   : GenPendiente
    Created on : 10-abr-2013, 22:15:42
    Author     : mlabrinb
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            @import url(<s:url value="/css/monitor.css"/>);
            @import url(<s:url value="/DataTables-1.9.4/media/css/jquery.dataTables_themeroller.css"/>);
        </style>       
	<link href="http://code.jquery.com/ui/1.9.0/themes/redmond/jquery-ui.css" rel="stylesheet" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.js"></script>
	<!--script src="http://code.jquery.com/ui/1.9.0/jquery-ui.min.js"></script-->
        <script type="text/javascript" language="javascript" src="<s:url value="/DataTables-1.9.4/media/js/jquery.dataTables.js"/>"></script>  
        <script type="text/javascript" language="javascript" src="<s:url value="/DataTables-1.9.4/media/js/jquery.dataTables.min.js"/>"></script>  

        <script type="text/javascript">
            $(document).ready(function() {
                $('#TblLocales').dataTable({
                    "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 2, 3 ] },
                    ],
                    "oLanguage": {
                        "oPaginate": {
                            "sFirst": "Primera",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior",
                            "sLast": "Ultima"
                        },
                        "sInfo": "Registros encontrados _TOTAL_, mostrando desde el _START_ al _END_",
                        "sEmptyTable": "No existen registros a mostrar",
                        "sSearch": "Filtro de Busqueda:",
                        "sLengthMenu": "Mostrando _MENU_ registros por p�gina",
                        "sInfoFiltered": "(filtrados desde _MAX_ registros)"
                    },
                    "sScrollY": "290px",
                    "bScrollCollapse": true,
                    "bPaginate": false,
                    "bJQueryUI": true,
                    "bFilter": true,
                    "bSort": true
                });
                
                $(".check_todosgen").click(function(event){
                     if($(this).is(":checked")) {
                                $(".cdkpen:checkbox:not(:checked)").attr("checked", "checked");
                         }else{
                                 $(".cdkpen:checkbox:checked").removeAttr("checked");
                         }
                   });
   

            });
            
            function SoloNumeros(e) {
                //if (oObjeto.value.substring(oObjeto.value.length-1,oObjeto.value.length) >= 0 ) {}
                //else { oObjeto.value = oObjeto.value.substring(0,oObjeto.value.length-1); }	
                tecla = (document.all) ? e.keyCode : e.which; // 2
                if (tecla == 8 || tecla == 0) return true; // 3
                patron = /\d/; // 4
                te = String.fromCharCode(tecla); // 5
                return patron.test(te); // 6
            }

            function OkSelSuc() {
                var ExistenSel = false;
                var sucursales = "";
                $(".cdkpen:checkbox:checked").each(function(){
                    ExistenSel = true;
                    if (sucursales !== "") sucursales += ",";
                    sucursales += $(this).val();
                    if (parseInt($("#numdias"+$(this).val()).val()) < 1) {
                        alert('Debe indicar un n�mero de d�as igual o superior a 1');
                        $("#numdias"+$(this).val()).focus();
                        return false;                    
                    }
                    sucursales += "-" + $("#numdias"+$(this).val()).val();
                });
                if (!ExistenSel) {
                    alert('Debe seleccionar alguna o varias sucursales');
                    return false;
                }
                if (confirm('Confirma que desea reprocesar los locales seleccionados ?'))
                    $.ajax({
                       type:'POST',
                       url: "<s:url value="/doGenPend"/>",
                       data: {
                         sucursales: sucursales
                       },
                       success: function (resultado) {
                          alert('Reproceso enviado exitosamente');
                       }
                    });                        
            }
            
        </script>
    </head>
    <body>
        <table width="100%" border="0">    
            <tbody>
                <tr>
                    <td width="60%" style="text-align:left">
                        <input name="Todos" type="checkbox" value="1" class="check_todosgen"/>Seleccionar todos
                    </td>                    
                    <td width="40%" style="text-align:right">
                        <input type="button" id="btnOk" onclick="javascript:OkSelSuc();" value="ejecutar" />
                    </td>
                </tr>            
            </tbody>
        </table>
        <div id="datos">
            <table width="100%" class="display" id="TblLocales">
                <thead>
                    <tr>
                        <th style="text-align: left">C&oacute;digo</th>
                        <th style="text-align: left">Nombre</th>
                        <th style="text-align: center">&nbsp;</th>
                        <th style="text-align: center">D&iacute;as</th>
                    </tr>
                </thead>
                <tbody>                
                <s:iterator value="myListSuc">
                <tr>
                    <td width="15%" style="text-align: left"><s:property value="id_suc" /></td>
                    <td width="70%" style="text-align: left"><s:property value="nom_suc" /></td>
                    <td width="5%" style="text-align: center">
                        <input type="checkbox" id="locales" value="<s:property value="id_suc" />" class="cdkpen" />
                    </td>
                    <td width="10%" style="text-align: center">
                        <input type="text" id="numdias<s:property value="id_suc" />" value="<s:property value="numdias" />" size="2" onkeypress="return SoloNumeros(event)" />
                    </td>
                </tr>
                </s:iterator>
                </tbody>
            </table>
        </div>
    </body>
</html>
