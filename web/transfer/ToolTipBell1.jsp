<%-- 
    Document   : ActualizaToolTipBell1
    Created on : 22-05-2013, 11:19:31 PM
    Author     : mlabrinb
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<s:if test="noleidosgen > 0">
    <s:set var="tooltip1" value="%{'Errores Generación, ' + noleidosgen + ' no leido(s)'}" />
</s:if>
<s:else>
    <s:set var="tooltip1" value="%{'Errores Generación'}" />
</s:else>
<span id="bell_gen"><img src="<s:url value="/icons/bell.png"/>" alt="" title="<s:property value="tooltip1"/>" /></span>
