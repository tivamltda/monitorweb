<%-- 
    Document   : TranPendientes
    Created on : 15-abr-2013, 12:26:42
    Author     : mlabrinb
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            @import url(<s:url value="/css/monitor.css"/>);
            @import url(<s:url value="/DataTables-1.9.4/media/css/jquery.dataTables_themeroller.css"/>);
        </style>       
	<link href="http://code.jquery.com/ui/1.9.0/themes/redmond/jquery-ui.css" rel="stylesheet" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.js"></script>
	<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.min.js"></script>
        <script type="text/javascript" language="javascript" src="<s:url value="/DataTables-1.9.4/media/js/jquery.dataTables.js"/>"></script>  

        <script type="text/javascript">
            $(document).ready(function() {
                $('#TblTraPendientes').dataTable({
                    "oLanguage": {
                        "oPaginate": {
                            "sFirst": "Primera",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior",
                            "sLast": "Ultima"
                        },
                        "sInfo": "Registros encontrados _TOTAL_, mostrando desde el _START_ al _END_",
                        "sEmptyTable": "No existen registros a mostrar",
                        "sSearch": "Filtro de Busqueda:",
                        "sLengthMenu": "Mostrando _MENU_ registros por p�gina",
                        "sInfoFiltered": "(filtrados desde _MAX_ registros)"
                    },
                    "sScrollY": "290px",
                    "bScrollCollapse": true,
                    "bPaginate": false,
                    "bJQueryUI": true,
                    "bFilter": true,
                    "bSort": true
                });
            });
            
        </script>
    </head>
    <body>
        <table width="100%" class="display" id="TblTraPendientes">
            <thead>
                <tr>
                    <th style="text-align: left">C&oacute;digo</th>
                    <th style="text-align: left">Nombre</th>
                    <th style="text-align: center">Estado<br/>Transmisi&oacute;n</th>
                </tr>
            </thead>
            <tbody>                
            <s:iterator value="myListSuc">
            <tr>
                <td width="15%" style="text-align: left"><s:property value="id_suc" /></td>
                <td width="70%" style="text-align: left"><s:property value="nom_suc" /></td>
                <td width="15%" style="text-align: center"><s:property value="est_trt" /></td>
            </tr>
            </s:iterator>
            </tbody>
        </table>
    </body>
</html>
