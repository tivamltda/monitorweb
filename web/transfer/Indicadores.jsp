<%-- 
    Document   : Status
    Created on : 16-abr-2013, 16:59:14
    Author     : mlabrinb
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<table border="0" width="300px" cellpadding="0" cellspacing="0" align="center" id="indicadores">
    <tr><td colspan="2" style="padding: 2px;">&nbsp;</td></tr>
    <tr>
        <td class="titulo">GENERA</td>
        <td class="valor"><span id="genera"><s:property value="genera"/></span></td>
    </tr>
    <tr><td colspan="2" style="padding: 2px;">&nbsp;</td></tr>
    <tr>
        <td class="titulo">VALIDA</td>
        <td class="valor"><span id="valida"><s:property value="valida"/></span></td>
    </tr>
    <tr><td colspan="2" style="padding: 2px;">&nbsp;</td></tr>
    <tr>
        <td class="titulo">TRANSF.</td>
        <td class="valor"><span id="transf"><s:property value="transf"/></span></td>
    </tr>
    <tr><td colspan="2" style="padding: 2px;">&nbsp;</td></tr>
</table>
