<%-- 
    Document   : ActualizaToolTipBell2
    Created on : 22-05-2013, 11:19:42 PM
    Author     : mlabrinb
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<s:if test="noleidosval > 0">
    <s:set var="tooltip2" value="%{'Errores Validación'} + noleidosval + ' no leido(s)'" />
</s:if>
<s:else>
    <s:set var="tooltip2" value="%{'Errores Validación'}" />
</s:else>
<span id="bell_val"><img id="bell_val" src="<s:url value="/icons/bell.png"/>" alt="" title="<s:property value="tooltip2"/>" /></span>
