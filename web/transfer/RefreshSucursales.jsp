<%-- 
    Document   : Status
    Created on : 16-abr-2013, 16:59:14
    Author     : mlabrinb
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<div id="datos">
    <table width="100%" class="display" id="TblLocales">
        <thead>
            <tr>
                <th style="text-align: left">C&oacute;digo</th>
                <th style="text-align: left">Nombre</th>
                <th style="text-align: center">&nbsp;</th>
                <th style="text-align: center">D&iacute;as</th>
            </tr>
        </thead>
        <tbody>                
        <s:iterator value="myListSuc">
        <tr>
            <td width="15%" style="text-align: left"><s:property value="id_suc" /></td>
            <td width="70%" style="text-align: left"><s:property value="nom_suc" /></td>
            <td width="5%" style="text-align: center">
                <input type="checkbox" id="locales" value="<s:property value="id_suc" />" class="cdkpen" />
            </td>
            <td width="10%" style="text-align: center">
                <input type="text" id="numdias<s:property value="id_suc" />" value="" size="2" onkeypress="return SoloNumeros(event)" />
            </td>
        </tr>
        </s:iterator>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    
    $(document).ready(function() {
        $('#TblLocales').dataTable({
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 2, 3 ] }
            ],
            "oLanguage": {
                "oPaginate": {
                    "sFirst": "Primera",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior",
                    "sLast": "Ultima"
                },
                "sInfo": "Registros encontrados _TOTAL_, mostrando desde el _START_ al _END_",
                "sEmptyTable": "No existen registros a mostrar",
                "sSearch": "Filtro de Busqueda:",
                "sLengthMenu": "Mostrando _MENU_ registros por p�gina",
                "sInfoFiltered": "(filtrados desde _MAX_ registros)"
            },
            "sScrollY": "290px",
            "bScrollCollapse": true,
            "bPaginate": false,
            "bJQueryUI": true,
            "bFilter": true,
            "bSort": true
        });
    });

</script>
