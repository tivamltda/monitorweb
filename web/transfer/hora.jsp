<%-- 
    Document   : hora
    Created on : 15-abr-2013, 18:33:00
    Author     : mlabrinb
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<table border="0" width="50%" cellpadding="0" cellspacing="0" align="center" id="fechayhora">
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
        <td class="titulo">HORA</td>
        <td class="valor"><s:property value="hora" /></td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
        <td class="titulo">FECHA</td>
        <td class="valor"><s:property value="fecha"/></td>
    </tr>
</table>
