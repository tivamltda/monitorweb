<%-- 
    Document   : Help
    Created on : 18-abr-2013, 16:12:07
    Author     : mlabrinb
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ayuda</title>
        <link href="<s:url value="/css/monitor.css"/>" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <p><s:property value="hlpsub"/></p>
    </body>
</html>
