<%-- 
    Document   : TransferSupport
    Created on : 10-abr-2013, 9:32:33
    Author     : mlabrinb
--%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <title>Monitor Web 1.0</title>
    <link href="<s:url value="/css/monitor.css"/>" rel="stylesheet" type="text/css" />
    <sj:head jqueryui="true" jquerytheme="redmond"/>
    
    <style>
        .titulo {
            font-size: .75em;
            font-family: Verdana, Helvetica, Sans-Serif;
            font-weight: bold;
            text-align: center; 
            background-color: darkseagreen; 
            color: white;
            border: solid black 1px;
            width: 170px;
            padding: 4px; 
        }

        .valor {
            font-size: .75em;
            font-family: Verdana, Helvetica, Sans-Serif;
            text-align: center; 
            background-color: aliceblue;
            color: black;
            border-top: solid black 1px;
            border-bottom: solid black 1px;
            border-right: solid black 1px;
            width: 170px;
            padding: 4px; 
        }
        .boton {
            text-align: center; 
            vertical-align: middle;
            cursor: url;
            width: 25px;
        }
    </style>
</head>

<body>
    
    <s:url var="linkgenera" action="MostrarPendGen"/>
    <sj:dialog id="dlgGenera" 
               href="%{linkgenera}" 
               title="PEND.GEN" 
               modal="true" 
               autoOpen="false" 
               height="460"
               width="600"
            />
    
    <s:url var="linktodas" action="MostrarTodas"/>
    <sj:dialog id="dlgTodas" 
               href="%{linktodas}" 
               title="TODAS" 
               modal="true" 
               autoOpen="false" 
               height="460"
               width="600"
            />
    
    <s:url var="linkerrval" action="MostrarErrVal"/>
    <sj:dialog id="dlgErrVal" 
               href="%{linkerrval}" 
               title="ERR.VAL" 
               modal="true" 
               autoOpen="false" 
               height="460"
               width="750"
            />
    
    <s:url var="linkerrgen" action="MostrarErrGen"/>
    <sj:dialog id="dlgErrGen" 
               href="%{linkerrgen}" 
               title="ERR.GEN" 
               modal="true" 
               autoOpen="false" 
               height="460"
               width="750"
            />
    
    <s:url var="linktrapen" action="MostrarTraPen"/>
    <sj:dialog id="dlgTraPen" 
               href="%{linktrapen}" 
               title="Estado Transmisiones" 
               modal="true" 
               autoOpen="false" 
               height="460"
               width="640"
            />

    <s:url var="lnkhelpgen" action="HelpGenera"/>
    <sj:dialog id="dlgHlpGen" 
               href="%{lnkhelpgen}" 
               title="AYUDA" 
               modal="true" 
               autoOpen="false" 
               height="280"
            />

    <s:url var="lnkhelpval" action="HelpValida"/>
    <sj:dialog id="dlgHlpVal" 
               href="%{lnkhelpval}" 
               title="AYUDA" 
               modal="true" 
               autoOpen="false" 
               height="280"
            />

    <s:url var="lnkhelptrans" action="HelpTrans"/>
    <sj:dialog id="dlgHlpTra" 
               href="%{lnkhelptrans}" 
               title="AYUDA" 
               modal="true" 
               autoOpen="false" 
               height="280"
            />

    <table border="0" width="100%" cellpadding="0" cellspacing="10" class="">
        <tr>
            <td width="50%" class="seccion">ESTADO GENERAL</td>
            <td class="seccion">GENERACION ARCHIVOS EN LOCAL</td>
        </tr>
        <tr>
            <td rowspan="5" valign="top">
                <table border="0" width="50%" cellpadding="0" cellspacing="0" align="center" id="fechayhora">
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td class="titulo">HORA</td>
                        <td class="valor"><s:property value="hora" /></td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td class="titulo">FECHA</td>
                        <td class="valor"><s:property value="fecha"/></td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <s:if test="noleidosgen > 0">
                    <s:set var="tooltip1" value="%{'Errores Generación, ' + noleidosgen + ' no leido(s)'}" />
                </s:if>
                <s:else>
                    <s:set var="tooltip1" value="%{'Errores Generación'}" />
                </s:else>
                <s:if test="noleidosval > 0">
                    <s:set var="tooltip2" value="%{'Errores Validación'} + noleidosval + ' no leido(s)'" />
                </s:if>
                <s:else>
                    <s:set var="tooltip2" value="%{'Errores Validación'}" />
                </s:else>
                <table border="0" width="450px" cellpadding="0" cellspacing="0" align="center">
                    <tr><td colspan="6" style="padding: 2px;">&nbsp;</td></tr>
                    <tr>
                        <td class="titulo">GENERA</td>
                        <td class="valor"><span id="genera"><s:property value="genera"/></span></td>
                        <td width="5px">&nbsp;</td>
                        <td class="boton" valign="middle"><sj:a openDialog="dlgGenera"><img src="<s:url value="/icons/24-message-info.png"/>" alt="" title="Generaci&oacute;n Pendiente" /></sj:a></td>
                        <td width="5px">&nbsp;</td>
                        <td class="boton" valign="middle"><sj:a openDialog="dlgErrGen"><span id="bell_gen"><img src="<s:url value="/icons/bell.png"/>" alt="" title="<s:property value="tooltip1"/>" /></span></sj:a></td>
                        <td width="5px">&nbsp;</td>
                        <td class="boton" valign="middle"><sj:a openDialog="dlgTodas"><img src="<s:url value="/icons/world.png"/>" alt="" title="Ver Todas" /></sj:a></td>
                        <td width="5px">&nbsp;</td>
                        <td class="boton"><sj:a openDialog="dlgHlpGen"><img src="<s:url value="/icons/24-message-warn.png"/>" alt="" title="Que hacer" /></sj:a></td>
                    </tr>
                    <tr><td colspan="6" style="padding: 2px;">&nbsp;</td></tr>
                    <tr>
                        <td class="titulo">VALIDA</td>
                        <td class="valor"><span id="valida"><s:property value="valida"/></span></td>
                        <td width="5px">&nbsp;</td>
                        <td class="boton"><sj:a openDialog="dlgErrVal"><span id="bell_val"><img src="<s:url value="/icons/bell.png"/>" alt="" title="<s:property value="tooltip2"/>" /></span></sj:a></td>
                        <td width="5px">&nbsp;</td>
                        <td class="boton"><sj:a openDialog="dlgHlpVal"><img src="<s:url value="/icons/24-message-warn.png"/>" alt="" title="Que hacer" /></sj:a></td>
                        <td width="5px" colspan="4">&nbsp;</td>
                    </tr>
                    <tr><td colspan="6" style="padding: 2px;">&nbsp;</td></tr>
                    <tr>
                        <td class="titulo">TRANSF.</td>
                        <td class="valor"><span id="transf"><s:property value="transf"/></span></td>
                        <td width="5px">&nbsp;</td>
                        <td class="boton"><sj:a openDialog="dlgTraPen"><img src="<s:url value="/icons/24-message-info.png"/>" alt="" title="Estado Transmisiones" /></sj:a></td>
                        <td width="5px">&nbsp;</td>
                        <td class="boton"><sj:a openDialog="dlgHlpTra"><img src="<s:url value="/icons/24-message-warn.png"/>" alt="" title="Que hacer" /></sj:a></td>
                        <td width="5px" colspan="4">&nbsp;</td>
                    </tr>
                    <tr><td colspan="8" style="padding: 2px;">&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="seccion">CARGA Y VALIDACION EN INT</td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td class="seccion">PENDIENTES</td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td colspan="2" class="seccion">INFORMACION GENERAL</td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="reporte" style="position:relative; width:100%; height: 100px; left: 0px; top: 0px; border: solid 1px">
                    <s:property value="logerr"/>
                </div>
            </td>
        </tr>
    </table>

</body>

</html>


