<%-- 
    Document   : monitor
    Created on : 09-abr-2013, 19:56:06
    Author     : mlabrinb
--%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Monitor v 1.0</title>
        <script type="text/javascript">
            fnCargarDiv = function (objetivo, url){           
                $(document).ready(function() {
                    $(objetivo).load(url);
                });
            };            
            
            fnAlarma = function () {
                $.ajax({
                   type:'POST',
                   url: "<s:url value="/ActualizaFlagAlarma"/>",
                   success : function(output) {
                      var noleidos = parseInt(output.replace(/^\s+/g,'').replace(/\s+$/g,''));
                      if (noleidos > 0) {
                          //alert('Alarma on');
                          $("#audio")[0].play();
                      }
                   }
                });                        
            };
            
            setInterval('fnCargarDiv("#fechayhora", "<s:url value="/ActualizaHora"/>")', 1000);
            setInterval('fnCargarDiv("#genera", "<s:url value="/ActualizaStatusGenera"/>")', 10000);
            setInterval('fnCargarDiv("#valida", "<s:url value="/ActualizaStatusValida"/>")', 10000);
            setInterval('fnCargarDiv("#transf", "<s:url value="/ActualizaStatusTrans"/>")', 10000);
            setInterval('fnCargarDiv("#bell_gen", "<s:url value="/ActualizaToolTipBell1"/>")', 10000);
            setInterval('fnCargarDiv("#bell_val", "<s:url value="/ActualizaToolTipBell2"/>")', 10000);
            setInterval('fnAlarma()', <s:property value="tiempoalarma" />);
        </script>
        <sj:head jquerytheme="redmond" />
    </head>    
    <body>
        
    <s:url var="remoteurl1" action="TransferSupport"/>
    <s:url var="remoteurl2" action="ajax2"/>
    <s:url var="remoteurl3" action="ajax3"/>
    <s:url var="remoteurl4" action="ajax4"/>
    <s:url var="remoteurl5" action="ajax5"/>
    <sj:tabbedpanel id="remotetabs" selectedTab="0" show="true" hide="'fade'" collapsible="true" sortable="true">
        <sj:tab id="tab1" href="%{remoteurl1}" label="TRANSFER.TIENDAS"/>
        <sj:tab id="tab2" href="%{remoteurl2}" label="REPLIC.MAESTROS" closable="true" />
        <sj:tab id="tab3" href="%{remoteurl3}" label="ACT.CONVENIOS" closable="true" />
        <sj:tab id="tab4" href="%{remoteurl4}" label="SAP<->TIENDAS" closable="true" />
        <sj:tab id="tab5" href="%{remoteurl5}" label="PEDIDOS PUSH" closable="true" />
    </sj:tabbedpanel>
    <audio id="audio">
        <source src="<s:url value="/sound/001082526_prev.mp3" forceAddSchemeHostAndPort="true" />" />
    </audio>    
    </body>
</html>
